const delay = timer => proxy => {
  var mockApi = {};
  Object.keys(proxy).forEach(function (key) {
    var result = proxy[key].$body || proxy[key];
    if (Object.prototype.toString.call(result) === '[object String]' && /^http/.test(result)) {
      mockApi[key] = proxy[key];
    } else {
      mockApi[key] = function (req, res) {
        var foo;
        if (Object.prototype.toString.call(result) === '[object Function]') {
          foo = result;
        } else {
          foo = function (req, res) {
            res.json(result);
          };
        }

        setTimeout(function () {
          foo(req, res);
        }, timer);
      };
    }
  });
  mockApi.__mockData = proxy;
  return mockApi;
};

const mockInterface = {
  'GET /api/crawlers': {
    success: true,
    data: [{
      key: '1',
      name: 'Crawler1',
      type: 'type1',
      args: [{
        key: 'key1',
        value: 'value1'
      }, {
        key: 'key2',
        value: 'value2'
      }],
      status: 'status1',
    }, {
      key: '2',
      name: 'Crawler2',
      type: 'type1',
      args: [{
        key: 'key1',
        value: 'value1'
      }, {
        key: 'key2',
        value: 'value2'
      }],
      status: 'status1',
    }, {
      key: '3',
      name: 'Crawler3',
      type: 'type2',
      args: null,
      status: 'status1',
    }]
  },
  
  'GET /api/crawlers/types': {
    success: true,
    data: [{
      type: 'Type1',
      value: 1,
      args: [{
        key: 'Key1',
        placeholder: 'placeholder for Key1',
        required: true
      }, {
        key: 'Key2',
        placeholder: 'placeholder for Key2',
        required: false
      }]
    }, {
      type: 'Type2',
      value: 2,
      args: [{
        key: 'Key1',
        placeholder: 'placeholder for Key1',
        required: true
      }]
    }]
  },

  'POST /api/crawlers': (req, res) => {
    res.send({
      success: true
    });
  }
}

export default delay(2000)(mockInterface);

const delay = timer => proxy => {
  var mockApi = {};
  Object.keys(proxy).forEach(function (key) {
    var result = proxy[key].$body || proxy[key];
    if (Object.prototype.toString.call(result) === '[object String]' && /^http/.test(result)) {
      mockApi[key] = proxy[key];
    } else {
      mockApi[key] = function (req, res) {
        var foo;
        if (Object.prototype.toString.call(result) === '[object Function]') {
          foo = result;
        } else {
          foo = function (req, res) {
            res.json(result);
          };
        }

        setTimeout(function () {
          foo(req, res);
        }, timer);
      };
    }
  });
  mockApi.__mockData = proxy;
  return mockApi;
};

const mockInterface = {
  'GET /api/dashboard/crawlers': {
    success: true,
    data: {
      succeeded: 20,
      failed: 40
    }
  },

  'GET /api/dashboard/stats': {
    success: true,
    data: [{
      day: "1",
      count: 4
    }, {
      day: "2",
      count: 3.5
    }, {
      day: "3",
      count: 5
    }, {
      day: "4",
      count: 4.9
    }, {
      day: "5",
      count: 6
    }, {
      day: "6",
      count: 7
    }, {
      day: "7",
      count: 9
    }, {
      day: "8",
      count: 13
    }]
  },

  'GET /api/dashboard/logs': {
    success: true,
    data: [{
      at: 1538825649412,
      log: 'this is log 1'
    }, {
      at: 1538826249412,
      log: 'this is log 2'      
    }, {
      at: 1538826250412,
      log: 'this is log 3'
    }, {
      at: 1538826549412,
      log: 'this is log 4'
    }, {
      at: 1538826649412,
      log: 'this is log 5'
    }, {
      at: 1538826649412,
      log: 'this is log 5'
    }, {
      at: 1538826649412,
      log: 'this is log 5'
    }, {
      at: 1538826649412,
      log: 'this is log 5'
    }, {
      at: 1538826649412,
      log: 'this is log 5'
    }, {
      at: 1538826649412,
      log: 'this is log 5'
    }, {
      at: 1538826649412,
      log: 'this is log 5'
    }, {
      at: 1538826649412,
      log: 'this is log 5'
    }, {
      at: 1538826649412,
      log: 'this is log 5'
    }, {
      at: 1538826649412,
      log: 'this is log 5'
    }, {
      at: 1538826649412,
      log: 'this is log 5'
    }, {
      at: 1538826649412,
      log: 'this is log 5'
    }, {
      at: 1538826649412,
      log: 'this is log 5'
    }]
  }
}

export default delay(2000)(mockInterface);

import React from 'react';
import styles from './logo.css';

export default () => <div className={styles.logo}>DM</div>;

import React from 'react';
import { Menu, Icon } from 'antd';
import Link from 'umi/link';

const { Item } = Menu;

export default ({ theme, location, menu }) => (
  <Menu
    theme={theme}
    mode="inline"
    selectedKeys={[location.pathname]}
  >
    {menu.map(item => 
      <Item key={item.to}>
        <Link to={item.to}>
          <Icon type={item.icon}/><span>{item.title}</span>
        </Link>
      </Item>
    )}
  </Menu>
);

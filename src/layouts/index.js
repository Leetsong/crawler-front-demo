import { Component } from 'react';
import { 
  Layout as AntdLayout, 
  Icon,
  Breadcrumb
} from 'antd';
import Link from 'umi/link';
import withRouter from 'umi/withRouter';
import Logo from './components/logo';
import Menu from './components/menu';
import styles from './index.css';
import MENU from './menu';

const {
  Header,
  Sider,
  Content,
  Footer
} = AntdLayout;

class Layout extends Component {

  state = {
    collapsed: false
  }

  onToggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  render() {
    const { children, location } = this.props;
    const { collapsed } = this.state;
    const pathnames = location.pathname.split('/');

    return (
      <AntdLayout className={styles.layout}>
        <Sider
          theme="light"
          trigger={null}
          collapsible={true}
          collapsed={collapsed}
        >
          <Link to="/"><Logo/></Link>
          <Menu menu={MENU} location={location} theme="light"/>
        </Sider>
        <AntdLayout>
          <Header className={styles.header}>
            <Icon 
              className={styles.trigger}
              type={collapsed ? 'menu-unfold' : 'menu-fold'} 
              onClick={this.onToggleCollapsed}
            />
            <Breadcrumb className={styles.breadcrumb}>
              { pathnames.map((p, i) =>
                <Breadcrumb.Item key={p}>
                  { i === pathnames.length - 1
                      ? p.toUpperCase()
                      : <Link to={pathnames.slice(0, i + 1).join('/')}>{p.toUpperCase()}</Link> 
                  }
                </Breadcrumb.Item>)
              }
            </Breadcrumb>
          </Header>
          <Content className={styles.content}>
            <div className={styles.children}>
              {children}
            </div>
            <Footer className={styles.footer}>
              Created by ICS@NJU
            </Footer>
          </Content>
        </AntdLayout>
      </AntdLayout>
    );
  }
}

export default withRouter(Layout);

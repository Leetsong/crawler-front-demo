const MENU = [{
    to: '/dashboard',
    icon: 'dashboard',
    title: 'Dashboard'
  }, {
    to: '/crawlers',
    icon: 'appstore',
    title: 'Crawlers'
}];

export default MENU;
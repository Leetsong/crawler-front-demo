export default ({ children, level }) => {
  switch (level) {
    case 1: return <h1 style={{ margin: 0 }}>{children}</h1>;
    case 2: return <h2 style={{ margin: 0 }}>{children}</h2>;
    case 3: return <h3 style={{ margin: 0 }}>{children}</h3>;
    case 4: return <h4 style={{ margin: 0 }}>{children}</h4>;
    case 5: return <h5 style={{ margin: 0 }}>{children}</h5>;
    default: return <h1 style={{ margin: 0 }}>{children}</h1>;
  }
}
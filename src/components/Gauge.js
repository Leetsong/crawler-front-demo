import PropTypes from 'prop-types';
import { 
  Chart,
  Axis,
  Geom,
  Coord,
  Guide,
  Shape
} from 'bizcharts';

const { Html, Arc } = Guide;

const INDICATOR_NAME = 'indicator';

// define the indicator
Shape.registerShape('point', INDICATOR_NAME, {
  drawShape(cfg, group) {
    let point = cfg.points[0]; // get indicator head
    point = this.parsePoint(point);
    const center = this.parsePoint({ x: 0, y: 0 }); // get the indicator tail
    // draw the indicator
    group.addShape('line', {
      attrs: {
        x1: center.x,
        y1: center.y,
        x2: point.x,
        y2: point.y,
        stroke: cfg.color,
        lineWidth: 5,
        lineCap: 'round',
      },
    });
    return group.addShape('circle', {
      attrs: {
        x: center.x,
        y: center.y,
        r: 12,
        stroke: cfg.color,
        lineWidth: 4.5,
        fill: '#fff',
      },
    });
  },
});

const COLOR_UNDER_FIT = '#0086FA';
const COLOR_VERY_FIT = '#FFBF00';
const COLOR_OVER_FIT = '#F5222D';

function Gauge({ value, title, minValue, tickInterval, maxValue, gaugeWidth, showPercentage, height, width, forceFit }) {
  const midValue13 = minValue + (maxValue - minValue) / 3;
  const midValue23 = minValue + (maxValue - minValue) / 3 * 2;
  const scale = {
    value: {
      min: minValue,
      max: maxValue,
      tickInterval: tickInterval,
      nice: false
    }
  };
  let renderTitle;

  if (showPercentage) {
    let percentage = ((value - minValue) / (maxValue - minValue) * 100).toFixed(2);
    if (percentage.endsWith('.00')) {
      percentage = percentage.substring(0, percentage.indexOf('.'));
    }
    renderTitle = () => `<div style="width: 300px; text-align: center; font-size: 12px!important;">
      <p style="font-size: 14px; color: rgba(0,0,0,0.43); margin: 0;">${title}</p>
      <p style="font-size: 25px; color: rgba(0,0,0,0.85); margin: 0;">${percentage}%</p>
    </div>`;
  } else {
    renderTitle = () => `<div style="width: 300px; text-align: center; font-size: 12px!important;">
      <p style="font-size: 14px; color: rgba(0,0,0,0.43); margin: 0;">${title}</p>
      <p style="font-size: 25px; color: rgba(0,0,0,0.85); margin: 0;">${value}</p>
    </div>`;
  }

  return (
    <Chart height={height} data={[{value: value}]} scale={scale} width={width} forceFit={forceFit}>
      <Coord type="polar" startAngle={-9 / 8 * Math.PI} endAngle={1 / 8 * Math.PI} radius={0.75} />
      <Axis
        name="value"
        zIndex={2}
        line={null}
        label={{
          offset: -20,
          textStyle: {
            fontSize: 12,
            fill: '#CBCBCB',
            textAlign: 'center',
            textBaseline: 'middle',
          },
        }}
        tickLine={{
          length: -24,
          stroke: '#fff',
          strokeOpacity: 1,
        }}
      />
      <Axis name="1" visible={false} />
      <Guide>
        <Arc
          zIndex={0}
          start={[minValue, 0.965]}
          end={[maxValue, 0.965]}
          style={{ // bottom grey
            stroke: 'rgba(0, 0, 0, 0.09)',
            lineWidth: gaugeWidth,
          }}
        />
        {value >= midValue13 && <Arc
          zIndex={1}
          start={[minValue, 0.965]}
          end={[midValue13, 0.965]}
          style={{
            stroke: COLOR_UNDER_FIT,
            lineWidth: gaugeWidth,
          }}
        />}
        { value >= midValue23 && <Arc
          zIndex={1}
          start={[midValue13, 0.965]}
          end={[midValue23, 0.965]}
          style={{
            stroke: COLOR_VERY_FIT,
            lineWidth: gaugeWidth,
          }}
        />}
        { value >= midValue23 && value <= maxValue && <Arc
          zIndex={1}
          start={[midValue23, 0.965]}
          end={[value, 0.965]}
          style={{
            stroke: COLOR_OVER_FIT,
            lineWidth: gaugeWidth,
          }}
        />}
        { value >= midValue13 && value < midValue23 && <Arc
          zIndex={1}
          start={[midValue13, 0.965]}
          end={[value, 0.965]}
          style={{
            stroke: COLOR_VERY_FIT,
            lineWidth: gaugeWidth,
          }}
        />}
        { value < midValue13 && <Arc
            zIndex={1}
            start={[minValue, 0.965]}
            end={[value, 0.965]}
            style={{
              stroke: COLOR_UNDER_FIT,
              lineWidth: gaugeWidth,
            }}
          />}
        <Html position={['50%', '95%']} html={renderTitle}/>
      </Guide>
      {/* indicator */}
      <Geom
        type="point"
        position="value*1"
        shape={INDICATOR_NAME}
        color="#1890FF"
        active={false}
        style={{ stroke: '#fff', lineWidth: 1 }}
      />
    </Chart>
  );
};

Gauge.propTypes = {
  value: PropTypes.number.isRequired,
  minValue: PropTypes.number,
  tickInterval: PropTypes.number,
  maxValue: PropTypes.number,
  title: PropTypes.string,
  showPercentage: PropTypes.bool,
  height: PropTypes.number,
  gaugeWidth: PropTypes.number
};

Gauge.defaultProps = {
  minValue: 0,
  tickInterval: 10,
  maxValue: 100,
  title: '',
  showPercentage: false,
  height: 400,
  width: 0,
  forceFit: true,
  gaugeWidth: 20
};

export default Gauge;

import {
  Chart,
  Axis,
  Geom,
  Tooltip
} from 'bizcharts';
import PropTypes from 'prop-types';

function LineChart({ data, xname, yname, xtitle, ytitle, lineWidth, circleSize, circleLineWidth, height, width, forceFit }) {
  
  const lineCharScale = {
    [xname]: { range: [0, 1], alias: xtitle },
    [yname]: { min: 0, alias: ytitle }
  };

  const showXTitle = !!xtitle ? true : false;
  const showYTitle = !!ytitle ? true : false;

  return (
    <Chart height={400} width={width} data={data} scale={lineCharScale} forceFit={forceFit}>
        <Axis name={xname} title={showXTitle}/>
        <Axis name={yname} title={showYTitle}/>
        <Tooltip crosshairs={{ type: 'y' }}/>
        <Geom type="line" position={`${xname}*${yname}`} size={lineWidth}/>
        <Geom 
          type="point"
          position={`${xname}*${yname}`}
          size={circleSize}
          shape="circle"
          style={{
            stroke: '#fff',
            lineWidth: {circleLineWidth}
          }}
        />
    </Chart> 
  );
}

LineChart.propTypes = {
  data: PropTypes.array.isRequired,
  xname: PropTypes.string,
  yname: PropTypes.string,
  xtitle: PropTypes.string,
  ytitle: PropTypes.string,
  lineWidth: PropTypes.number,
  circleSize: PropTypes.number,
  circleLineWidth: PropTypes.number,
  height: PropTypes.number,
  width: PropTypes.number,
  forceFit: PropTypes.bool
};

LineChart.defaultProps = {
  xname: 'x',
  yname: 'y',
  xtitle: undefined,
  ytitle: undefined,
  lineWidth: 2,
  circleSize: 4,
  circleLineWidth: 1,
  height: 400,
  width: 0,
  forceFit: true
};

export default LineChart;

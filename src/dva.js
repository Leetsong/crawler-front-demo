import { message } from 'antd';

export default {
  config() {
    return {
      onError(err) {
        err.preventDefault();
        message.error(err.message);
      },
    };
  },
}

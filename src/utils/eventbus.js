function EventBus() {
  this.eventListeners = new Set();
}

EventBus.prototype.register = function(listener) {
  this.eventListeners.add(listener);
};

EventBus.prototype.unregister = function(listener) {
  this.eventListeners.delete(listener);
}

EventBus.prototype.dispatch = function(event) {
  if (!event || !event.type) {
    console.warn('Every event should not be null, and should have a `type` field');
  } else {
    this.eventListeners.forEach(listener => listener(event));
  }
}

const globalEventBus = new EventBus();

export default globalEventBus;

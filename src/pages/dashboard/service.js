import request from '@/utils/request';

export async function fetchCrawlers() {
  let res = await request('/api/dashboard/crawlers');
  return res.body;
}

export async function fetchStats() {
  let res = await request('/api/dashboard/stats');
  return res.body;
}

export async function fetchLogs() {
  let res = await request('/api/dashboard/logs');
  return res.body;
}

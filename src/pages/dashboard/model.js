import * as service from './service';

const DASHBOARD_CRAWLERS_FETCH_SUCCEEDED = 'DASHBOARD_CRAWLERS_FETCH_SUCCEEDED';
const DASHBOARD_CRAWLERS_FETCH_FAILED = 'DASHBOARD_CRAWLERS_FETCH_FAILED'
const DASHBOARD_STATS_FETCH_SUCCEEDED = 'DASHBOARD_STATS_FETCH_SUCCEEDED';
const DASHBOARD_STATS_FETCH_FAILED = 'DASHBOARD_STATS_FETCH_FAILED'
const DASHBOARD_LOGS_FETCH_SUCCEEDED = 'DASHBOARD_LOGS_FETCH_SUCCEEDED';
const DASHBOARD_LOGS_FETCH_FAILED = 'DASHBOARD_LOGS_FETCH_FAILED'

export default {
  namespace: 'dashboard',

  state: {
    crawlers: {
      loading: true,
      data: {}
    },

    logs: {
      loading: true,
      data: []
    },

    stats: {
      loading: true,
      data: []
    }
  },

  reducers: {
    'save-crawlers' (state, { payload: data }) {
      return {
        ...state,
        crawlers: {
          ...state.crawlers,
          loading: false,
          data
        }
      };
    },
    'save-stats' (state, { payload: data }) {
      return {
        ...state,
        stats: {
          ...state.stats,
          loading: false,
          data
        }
      };
    },
    'save-logs' (state, { payload: data }) {
      return {
        ...state,
        logs: {
          ...state.logs,
          loading: false,
          data
        }
      };
    },
    'set-crawlers-loading' (state, { payload: value }) {
      return {
        ...state,
        crawlers: {
          ...state.crawlers,
          loading: !!value ? true : false
        }
      }
    },
    'set-stats-loading' (state, { payload: value }) {
      return {
        ...state,
        stats: {
          ...state.stats,
          loading: !!value ? true : false
        }
      }
    },
    'set-logs-loading' (state, { payload: value }) {
      return {
        ...state,
        logs: {
          ...state.logs,
          loading: !!value ? true : false
        }
      }
    }
  },

  effects: {
    *'fetch-crawlers'(_, { call, put }) {
      let result = yield call(service.fetchCrawlers);
      if (result.success) {
        yield put({
          type: 'save-crawlers',
          payload: result.data
        });
        return DASHBOARD_CRAWLERS_FETCH_SUCCEEDED;
      } else {
        yield put({
          type: 'set-crawlers-loading',
          payload: false
        })
        return DASHBOARD_CRAWLERS_FETCH_FAILED;
      }
    },
    *'fetch-stats'(_, { call, put }) {
      let result = yield call(service.fetchStats);
      if (result.success) {
        yield put({
          type: 'save-stats',
          payload: result.data
        });
        return DASHBOARD_STATS_FETCH_SUCCEEDED;
      } else {
        yield put({
          type: 'set-stats-loading',
          payload: false
        })
        return DASHBOARD_STATS_FETCH_FAILED;
      }
    },
    *'fetch-logs'(_, { call, put }) {
      let result = yield call(service.fetchLogs);
      if (result.success) {
        yield put({
          type: 'save-logs',
          payload: result.data
        });
        return DASHBOARD_LOGS_FETCH_SUCCEEDED;
      } else {
        yield put({
          type: 'set-logs-loading',
          payload: false
        })
        return DASHBOARD_LOGS_FETCH_FAILED;
      }
    }
  }
}
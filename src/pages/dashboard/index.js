import { Component } from 'react';
import {
  Row,
  Col,
  Card,
  Timeline
} from 'antd';
import { connect } from 'dva';
import Gauge from '@/components/Gauge';
import LineChart from '@/components/LineChart';

@connect(({ dashboard }) => ({ dashboard }))
class Dashboard extends Component {

  componentDidMount() {
    this.props.dispatch({
      type: 'dashboard/fetch-crawlers'
    });
    this.props.dispatch({
      type: 'dashboard/fetch-logs'
    });
    this.props.dispatch({
      type: 'dashboard/fetch-stats'
    });
  }

  render() {
    const {
      crawlers: {
        loading: loadingCrawlers,
        data: crawlers
      },
      stats: {
        loading: loadingStats,
        data: stats
      },
      logs: {
        loading: loadingLogs,
        data: logs
      }
    } = this.props.dashboard;

    return (
      <div>
        <Row gutter={24}>
          <Col span={12}>
            <Card title="Crawlers" style={{ width: '100%' }} loading={loadingCrawlers}>
              <Gauge 
                  value={crawlers.succeeded / (crawlers.succeeded + crawlers.failed) * 100}
                  height={400}
                  title="succeeded"
                  showPercentage
              />
            </Card>
          </Col>
          <Col span={12}>
            <Card title="Logs" style={{ width: '100%' }} loading={loadingLogs}>
              <Timeline style={{ height: 405, overflow: 'auto' }}>
                { logs.map(l => 
                  <Timeline.Item key={l.at}>[{new Date(l.at).toLocaleDateString()}] {l.log}</Timeline.Item>
                ) }
              </Timeline>
          </Card>
          </Col>
        </Row>
        <Row style={{ marginTop: 24 }}>
          <Card title="Trend" style={{ width: '100%' }} loading={loadingStats}>
            <LineChart 
              data={stats}
              xname="day"
              yname="count"
              xtitle="day"
              ytitle="count crawled"
              height={400}
            />
          </Card>
        </Row>
      </div>
    );
  }
}

export default Dashboard;

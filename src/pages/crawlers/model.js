import * as service from './service';

export const CRAWLERS_FETCH_FAILED = 'CRAWLERS_FETCH_FAILED';
export const CRAWLERS_FETCH_SUCCEEDED = 'CRAWLERS_FETCH_SUCCEEDED';
export const CRAWLERS_TYPES_FETCH_SUCCEEDED = 'CRAWLERS_TYPES_FETCH_SUCCEEDED';
export const CRAWLERS_TYPES_FETCH_FAILED = 'CRAWLERS_TYPES_FETCH_FAILED';
export const CRAWLERS_DEPLOY_SUCCEEDED = 'CRAWLERS_DEPLOY_SUCCEEDED';
export const CRAWLERS_DEPLOY_FAILED = 'CRAWLERS_DEPLOY_FAILED';

export default {
  namespace: 'crawlers',

  state: {
    loading: true,
    data: [],
    total: 0,
    types: [],
    deploying: false
  },

  reducers: {
    'set-loading'(state, { payload: loading }) {
      return {
        ...state,
        loading: !!loading ? true : false
      };
    },
    'set-deploying'(state, { payload: deploying }) {
      return {
        ...state,
        deploying: !!deploying ? true : false
      };
    },
    'save-data'(state, { payload: { data } }) {
      return {
        ...state,
        data,
        loading: false
      };
    },
    'save-types'(state, { payload: { types }}) {
      return {
        ...state,
        types
      };
    }
  },

  effects: {
    *'fetch'(_, { call, put }) {
      const ret = yield call(service.fetch);
      if (ret.success) {
        yield put({
          type: 'save-data',
          payload: {
            data: ret.data,
            total: ret.total
          }
        });
        return CRAWLERS_FETCH_SUCCEEDED;
      } else {
        yield put({
          type: 'set-loading',
          payload: false
        });
        return CRAWLERS_FETCH_FAILED;
      }
    },
    *'fetch-types'(_, { call, put }) {
      const ret = yield call(service.fetchTypes);
      if (ret.success) {
        yield put({
          type: 'save-types',
          payload: {
            types: ret.data
          }
        });
        return CRAWLERS_TYPES_FETCH_SUCCEEDED;
      } else {
        return CRAWLERS_TYPES_FETCH_FAILED;
      }
    },
    *'deploy'({ payload }, { call, put }) {
      yield put({
        type: 'set-deploying',
        payload: true
      });

      const ret = yield call(service.deploy, payload);
      yield put({
        type: 'set-deploying',
        payload: false
      });

      if (!ret.success) {
        return CRAWLERS_DEPLOY_FAILED;
      } else {
        return CRAWLERS_DEPLOY_SUCCEEDED;
      }
    }
  }
}
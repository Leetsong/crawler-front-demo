import { Component } from 'react';
import { 
  Form, 
  Select, 
  Input,
  Button,
  message
} from 'antd';
import { connect } from 'dva';
import router from 'umi/router';
import CronBuilder from 'react-cron-builder';
import 'react-cron-builder/dist/bundle.css';
import Title from '@/components/Title';
import {
  CRAWLERS_TYPES_FETCH_FAILED,
  CRAWLERS_DEPLOY_SUCCEEDED,
  CRAWLERS_DEPLOY_FAILED
} from '../model';

@connect(({ crawlers }) => ({
  types: crawlers.types,
  deploying: crawlers.deploying
}))
class DeployPage extends Component {

  state = {
    selectedTypeValue: -1
  };

  onSelectType = value => {
    this.setState({
      ...this.state,
      selectedTypeValue: value
    })
  };

  onSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.dispatch({
          type: 'crawlers/deploy',
          payload: {
            type: this.state.selectedTypeValue,
            args: values
          }
        }).then(r => {
          if (r === CRAWLERS_DEPLOY_SUCCEEDED) {
            this.onCrawlerDeploySucceeded();
          } else if (r === CRAWLERS_DEPLOY_FAILED) {
            this.onCrawlerDeployFailed();
          }
        })
      }
    })
  };

  onCrawlerDeploySucceeded = () => {
    message.info('Success');
    router.replace('/crawlers');
  }

  onCrawlerDeployFailed = () => {
    message.error('Failed to deploy the crawler');
  }

  componentDidMount() {
    this.props.dispatch({
      type: 'crawlers/fetch-types'
    }).then(r => {
      if (r === CRAWLERS_TYPES_FETCH_FAILED) {
        message.error('Failed to fetch crawler types');
      }
    });
  }

  render() {
    const formLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 },
      }
    };
    const { types, deploying } = this.props;
    const { selectedTypeValue } = this.state;
    const { getFieldDecorator } = this.props.form;
    const args = selectedTypeValue > 0
                  ? types.filter(t => t.value === selectedTypeValue)[0].args
                  : null
    const disabled = !!!args;

    return (
      <div>
        <Title level={1}>Deploy a Crawler</Title>
        <Form onSubmit={this.onSubmit}>
          <Form.Item {...formLayout} label="Type">
            <Select onChange={this.onSelectType} placeholder="select a type">
              { types.map(type => <Select.Option key={type.value} value={type.value}>{type.type}</Select.Option>) }
            </Select>
          </Form.Item>
          { args ? args.map(a => 
              <Form.Item {...formLayout} key={a.key} label={a.key}>
                {getFieldDecorator(a.key, {
                  rules: [{ required: a.required || false }]
                })(
                  <Input placeholder={a.placeholder}/>
                )}
              </Form.Item>
            ) : null }
            <Form.Item {...formLayout} label="Cron">
              <CronBuilder
                cronExpression="*/4 2,12,22 * * 1-5"
                onChange={::console.log}
                showResult={false}
              />
            </Form.Item>
          <Button htmlType="submit" disabled={disabled} loading={deploying}>Deploy</Button>
        </Form>
      </div>
    );
  }
}

export default Form.create()(DeployPage);

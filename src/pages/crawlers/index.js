import { Component } from 'react';
import {
  Table,
  Button,
  Row,
  Col,
  message
} from 'antd';
import { connect } from 'dva';
import Link from 'umi/link';
import Title from '@/components/Title';
import { CRAWLERS_FETCH_FAILED } from "./model";

const COLUMNS = [{
  title: 'Name',
  dataIndex: 'name',
  key: 'name',
}, {
  title: 'Type',
  dataIndex: 'type',
  key: 'type',
}, {
  title: 'Args',
  dataIndex: 'args',
  key: 'args',
  render: args => !!args 
                    ? <div>{ args.map(arg => `${arg.key}=${arg.value}`).join(';') }</div>
                    : null
}, {
  title: 'Status',
  key: 'status',
  dataIndex: 'status'
}];

@connect(({ crawlers }) => ({ crawlers }))
class Crawlers extends Component {

  componentDidMount() {
    this.props.dispatch({
      type: 'crawlers/fetch'
    }).then(r => {
      if (r === CRAWLERS_FETCH_FAILED) {
        message.error('Failed to fetch crawlers');
      }
    });
  }

  render() {
    const { data, loading } = this.props.crawlers;

    return (
      <div>
        <Row>
          <Col span={8}>
            <Title level={1}>Crawler List</Title>
          </Col>
          <Col span={16} style={{ textAlign: 'right' }}>
            <Button><Link to="crawlers/deploy">Deploy</Link></Button>
          </Col>
        </Row>
        <Table loading={loading} columns={COLUMNS} dataSource={data}/>
      </div>
    );
  }
}

export default Crawlers;

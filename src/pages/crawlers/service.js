import request from '@/utils/request';

export async function fetch() {
  let res = await request('/api/crawlers');
  if (res.headers && res.headers['x-total-count']) {
    res.body['x-total-count'] = res.headers['x-total-count'];
  }
  return res.body;
}

export async function fetchTypes() {
  let res = await request('/api/crawlers/types');
  return res.body;
}

export async function deploy(crawler) {
  console.log(crawler);
  let res = await request('/api/crawlers', {
    method: 'POST',
    body: JSON.stringify(crawler)
  });
  return res.body;
}
